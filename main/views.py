from django.shortcuts import render
from django.utils import timezone

from main.models import Question


def post_list(request):
    questions = Question.objects.filter(created_date__lte=timezone.now()).order_by('created_date')
    return render(request, 'main/main_list.html', {'questions': questions})